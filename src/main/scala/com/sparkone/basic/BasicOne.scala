package com.sparkone.basic

import org.apache.spark._
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions

object BasicOne {
  def main(args: Array[String]) {
    val conf = new  SparkConf().setAppName("basicOne").setMaster("local[2]")
    val sc = new SparkContext(conf);
    val data = Array(1, 2, 3, 1, 3, 6, 9, 9, 9)
    val disData = sc.parallelize(data)
    disData.collect().foreach(println)
    val reducedData = disData.map(data => (data, 1)).reduceByKey(_ + _).collect();
    reducedData.foreach(println)
    
    println("########################################################")
    reducedData.filter(p=> p._1 % 2 != 0).foreach(println)
  }
}